const mix = require('laravel-mix');
const publicPath = "assets/dist";
const resourcesPath = "../";

// Set mix paths config
mix.setPublicPath(publicPath);
mix.setResourceRoot(resourcesPath);

// Add assets for mix to compile
mix.sass(
  'assets/scss/timeline.scss',
  'css'
);

if (mix.inProduction()) {
  mix.version();
}
else {
  mix.sourceMaps().webpackConfig({
    devtool: 'source-map',
  });
}
