(function ($, Drupal, drupalSettings, window) {

  "use strict";


  // Binding drupal ajax to dynamic links
  $.fn.attachAjaxToDynamicLink = function() {
    var base = $(this).attr('id');
    var elementSettings = {
      base: base,
      progress : { 'type': 'throbber' },
      url : $(this).attr('href'),
      event : 'click'
    };
    elementSettings.selector = '#' + base;
    $(elementSettings.selector).once('drupal-ajax').each(function () {
      elementSettings.element = this;
      elementSettings.base = base;
      Drupal.ajax(elementSettings);
    });
  }


  // Reset form.
  var _resetForm = function () {
    // Reset form.
    document.getElementById('frontend-status-add-form').reset();
    // Remove image.
    $('[name="image_remove_button"]').trigger('click');
    $('.status-container .image-wrapper').removeClass('hide-plus')
      .removeAttr('style');
  }

  /**
   * Drupal Ajax behaviours and ajax prototypes
   * @type {{attach: attach, detach: detach}}
   */
  Drupal.behaviors.timeline = {
    attach: function (context, settings) {

      //add image to background
      var imageWrapper = $('.status-container .image-wrapper');
      if (imageWrapper.find('span.file--image').length > 0) {
        var image_src = imageWrapper.find('span.file--image a').attr('href');
        if (image_src) {
          imageWrapper
            .addClass('hide-plus')
            .css('background-image', 'url(' + image_src + ')');
        }
      }
      else {
        imageWrapper
          .removeClass('hide-plus')
          .removeAttr('style');
      }

      // Add image.
      var imageButton = $('.action-add-photo');
      if (imageButton.length) {
        imageButton.once().on('click', function (e) {
          imageWrapper.toggleClass('js-active');
        });
      }

      // Add video button.
      var videoButton = $('.action-add-video');
      if (videoButton.length) {
        videoButton.once().on('click', function (e) {
          $('.status-container .form-item-video').toggleClass('js-active');
        });
      }

      // Update timeline.
      Drupal.AjaxCommands.prototype.updateTimelineCommand = function (ajax, response, status) {
        if (response.status) {
          $(response.wrapper).html(response.html);
          $(response.wrapper).find('.action-delete').attachAjaxToDynamicLink();
          _resetForm();
        }
      }
    },
    detach: function (context) {
    }
  };

}(jQuery, Drupal, drupalSettings, window));
