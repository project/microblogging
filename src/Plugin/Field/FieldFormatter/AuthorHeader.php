<?php

namespace Drupal\microblogging\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\microblogging\MicroBloggingHelper;

/**
 * Plugin implementation of the 'author_header' formatter.
 *
 * @FieldFormatter(
 *   id = "author_header",
 *   label = @Translation("Author Header"),
 *   description = @Translation("Display the referenced author user entity."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class AuthorHeader extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Todo: Below code is not independent.

    // Status.
    $status = $items->getEntity();

    // Show admin actions.
    $show_admin_actions = \Drupal::currentUser()
        ->id() == $status->getOwnerId();

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      /** @var $referenced_user \Drupal\user\UserInterface */

      // Time ago
      $ago = MicroBloggingHelper::getDaysAgo($status->getCreatedTime());

      $elements[$delta] = [
        '#theme' => 'author_header',
        '#author' => $entity,
        '#picture' => MicroBloggingHelper::getUserPictureOrNameAbbr($entity),
        '#name' => MicroBloggingHelper::getUserFullName($entity),
        '#ago' => $ago,
        '#show_actions' => $show_admin_actions,
        '#id' => $status->id(),
        '#cache' => [
          'tags' => $entity->getCacheTags(),
        ],
      ];
    }

    return $elements;
  }

}
