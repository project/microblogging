<?php

namespace Drupal\microblogging\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\microblogging\Ajax\UpdateTimelineCommand;
use Drupal\microblogging\Entity\StatusInterface;
use Drupal\microblogging\MicroBloggingHelper;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\microblogging\Form\Frontend\StatusAddForm;

/**
 * Class TimelineController
 *
 * @package Drupal\microblogging\Controller
 */
class TimelineController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * TimelineController constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Render\Renderer $renderer
   */
  public function __construct(RouteMatchInterface $routeMatch,
                              AccountProxyInterface $account,
                              FormBuilderInterface $formBuilder,
                              EntityTypeManagerInterface $entityTypeManager,
                              Renderer $renderer) {
    $this->routeMatch = $routeMatch;
    $this->currentUser = User::load($account->id());
    $this->formBuilder = $formBuilder;
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('form_builder'),
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Timeline view.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return array
   */
  public function view(UserInterface $user) {

    $form = NULL;
    if ($this->currentUser->hasPermission('add status entities') &&
      $this->currentUser->id() == $user->id()) {
      $form = $this->formBuilder->getForm(StatusAddForm::class);
    }

    // Load status
    $data = MicroBloggingHelper::getStatus($user->id());

    // Return theme.
    return [
      '#theme' => 'status_timeline',
      '#form' => $form,
      '#data' => $data['data'],
      '#pager' => $data['pager'],
      '#attached' => [
        'library' => [
          'microblogging/timeline',
        ],
      ],
    ];

  }

  /**
   * Delete status.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   */
  public function delete(Request $request) {

    $response = new AjaxResponse();

    $status = $this->routeMatch->getParameter('status');

    if ($status instanceof StatusInterface &&
      $status->getOwnerId() == $this->currentUser->id()) {

      try{
        // Delete status.
        $status->delete();

        // Update timeline.
        $build = MicroBloggingHelper::getStatus($this->currentUser->id());
        $html = $this->renderer->render($build);
        $response->addCommand(new UpdateTimelineCommand(TRUE, '#timeline-items', $html));

      } catch (\Exception $e){

      }
    }

    return $response;
  }

}
