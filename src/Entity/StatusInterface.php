<?php

namespace Drupal\microblogging\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Status entities.
 *
 * @ingroup microblogging
 */
interface StatusInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Status name.
   *
   * @return string
   *   Name of the Status.
   */
  public function getName();

  /**
   * Sets the Status name.
   *
   * @param string $name
   *   The Status name.
   *
   * @return \Drupal\microblogging\Entity\StatusInterface
   *   The called Status entity.
   */
  public function setName($name);

  /**
   * Gets the Status creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Status.
   */
  public function getCreatedTime();

  /**
   * Sets the Status creation timestamp.
   *
   * @param int $timestamp
   *   The Status creation timestamp.
   *
   * @return \Drupal\microblogging\Entity\StatusInterface
   *   The called Status entity.
   */
  public function setCreatedTime($timestamp);

}
