<?php

namespace Drupal\microblogging\Form\Frontend;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;
use Drupal\microblogging\Ajax\UpdateTimelineCommand;
use Drupal\microblogging\Entity\Status;
use Drupal\microblogging\MicroBloggingHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class StatusAddForm
 *
 * @package Drupal\microblogging\Form\Frontend
 */
class StatusAddForm extends FormBase {

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Class constructor.
   */
  public function __construct(AccountInterface $account,
                              RouteMatchInterface $routeMatch,
                              Renderer $renderer) {
    $this->account = $account;
    $this->routeMatch = $routeMatch;
    $this->renderer = $renderer;

    // User user from route.
    $this->user = $this->routeMatch->getParameter('user');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'frontend_status_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['form-container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['status-form'],
      ],
    ];

    // Header.
    $form['form-container']['header'] = [
      '#markup' => $this->getFormHeaderMarkup(),
    ];

    $form['form-container']['status-container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['status-container'],
      ],
    ];

    $form['form-container']['status-container']['status'] = [
      '#type' => 'textarea',
      '#title' => '',
      '#prefix' => '<div class="status-text">',
      '#suffix' => '</div>',
      '#attribute' => [
        'placeholder' => 'Post your status',
      ],
      '#resizable' => FALSE,
    ];

    // Image.
    $form['form-container']['status-container']['image-wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['image-wrapper'],
      ],
    ];

    $form['form-container']['status-container']['image-wrapper']['image'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://status/',
      '#multiple' => FALSE,
      '#description' => t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000],
      ],
      '#title' => t('Upload an image'),
    ];

    // Video.
    $form['form-container']['status-container']['video'] = [
      '#type' => 'textfield',
      '#title' => '',
      '#attributes' => [
        'class' => ['video-url'],
        'placeholder' => "Youtube of Vimeo url",
      ],
    ];

    // Footer.
    $form['form-container']['footer'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-footer'],
      ],
    ];

    // Form actions.
    $form['form-container']['footer']['actions']['#type'] = 'actions';
    $form['form-container']['footer']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Post'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::postStatus',
        'wrapper' => 'timeline-items',
      ],
    ];
    return $form;
  }

  /**
   * Return markup of header
   *
   * @return string
   */
  private function getFormHeaderMarkup(): string {
    $markup = '<header class="timeline-header ">
                    <span class="timeline-title">Add your status</span>';
    $markup .= '<div class="timeline-action">
          <a class="action-add-photo">Add image</a>
          <a class="action-add-video">Add video</a>
      </div>';
    $markup .= '</header>';

    return $markup;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Set error if all fields are empty.
    $status = $form_state->getValue('status');
    $image = $form_state->getValue('image');
    $video = $form_state->getValue('video');
    if (empty(trim($status)) && empty($image) && empty($video)) {
      $form_state->setErrorByName('status', 'Status must have something.');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Post status.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function postStatus(array &$form, FormStateInterface $form_state) {

    // Ajax response.
    $response = new AjaxResponse();

    // If current user is logged in.
    if ($this->account->hasPermission('add status entities') &&
      empty($form_state->getErrors())) {

      $user_id = $this->account->id();
      $body = $form_state->getValue('status');
      $image = $form_state->getValue('image');
      $video = $form_state->getValue('video');

      $status = Status::create([
        'name' => 'Status - ' . date('Y-m-d') . ' User - #' . $user_id,
        'field_body' => Xss::filter($body),
        'field_image' => $image,
        'field_video' => $video,
        'user_id' => $user_id,
        'status' => TRUE,
      ]);
      $status->save();

      // save comment image permanent
      if (!empty($image)) {
        $_image = File::load($image[0]);
        if (is_object($_image)) {
          $_image->setPermanent();
          $_image->save();
        }
      }

      // Add html.
      $build = MicroBloggingHelper::getStatus($this->user->id());
      $html = $this->renderer->render($build);

      $response->addCommand(new UpdateTimelineCommand(TRUE, '#timeline-items', $html));

    }

    return $response;
  }

}
