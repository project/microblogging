<?php

namespace Drupal\microblogging\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Status entities.
 *
 * @ingroup microblogging
 */
class StatusDeleteForm extends ContentEntityDeleteForm {


}
