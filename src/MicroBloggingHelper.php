<?php

namespace Drupal\microblogging;

use Drupal\image\Entity\ImageStyle;
use Drupal\microblogging\Entity\Status;
use Drupal\user\UserInterface;

class MicroBloggingHelper {

  public static $num_per_page = 10;

  /**
   * Get status for a user.
   *
   * @param $user_id
   *
   * @return null[]|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getStatus($user_id) {

    if (!$user_id) {
      return NULL;
    }

    // Output
    $output = [
      'data' => NULL,
      'pager' => NULL,
    ];

    //Get object of entity type manager.
    $entityTypeManager = \Drupal::entityTypeManager();

    // Get current page.
    $page = \Drupal::request()->query->get('page');
    $start_index = empty($page) ? 0 : (self::$num_per_page * $page);


    // Get query.
    $query = $entityTypeManager->getStorage('status')
      ->getQuery();

    $entityQuery = $query->condition('user_id', $user_id)
      ->condition('status', 1);

    // Get total count.
    $countQuery = clone $entityQuery;
    $total = $countQuery->count()->execute();

    // Set range.
    $status = $entityQuery->range($start_index, self::$num_per_page)
      ->sort('id', 'DESC')
      ->execute();

    if (!empty($status)) {

      // Load all status.
      $status = Status::loadMultiple($status);

      foreach ($status as $key => $_item) {
        if ($view = $entityTypeManager->getViewBuilder('status')
          ->view($_item)) {
          $view['#prefix'] = "<div class='timeline-item timeline-item-{$_item->id()}'><span class='history-timeline top'></span>";
          $view['#suffix'] = "<span class='history-timeline alt bottom'></span></div>";
          $status[$key] = $view;
        }
        else {
          unset($status[$key]);
        }
      }
    }

    // Set output data.
    $output['data'] = $status;

    // Prepare pager.
    $pager = \Drupal::service('pager.manager')
      ->createPager($total, self::$num_per_page);
    $pager = $pager->getCurrentPage();
    $output['pager'] = [
      '#type' => 'pager',
      '#prefix' => '<div class="timeline-pager">',
      '#suffix' => '</div>',
    ];

    // Return output.
    return $output;
  }

  /**
   * Return user picture url of name abbr.
   *
   * @param $user
   *
   * @return mixed|string
   */
  public static function getUserPictureOrNameAbbr(UserInterface $user) {
    $output = [
      'abbr' => FALSE,
    ];

    $user_picture = NULL;
    if ($file = $user->user_picture->entity) {
      $user_picture = ImageStyle::load('thumbnail')
        ->buildUri($file->getFileUri());
      $output['url'] = file_create_url($user_picture);
    }

    if (!$user_picture) {
      $output['abbr'] = TRUE;
      $output['name'] = $user->getDisplayName()[0];
    }

    return $output;
  }


  /**
   * Send full name of user.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return string
   */
  public static function getUserFullName(UserInterface $user) {
    // Set default account name.
    $name = $user->getAccountName();
    return $name;
  }

  /**
   * Get days ago.
   *
   * @param $ptime
   *
   * @return int|string
   */
  public static function getDaysAgo($ptime) {

    $time_val = 0;
    if ($ptime) {

      $estimate_time = time() - $ptime;

      if ($estimate_time < 1) {
        return 'less than 1 second ago';
      }

      $condition = [
        12 * 30 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second',
      ];

      foreach ($condition as $secs => $str) {
        $d = $estimate_time / $secs;

        if ($d >= 1) {
          $r = round($d);
          return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
        }
      }
    }
    return $time_val;
  }

}
