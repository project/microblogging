<?php

namespace Drupal\microblogging\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Defines a ajax command for updating the timeline.
 *
 * Class UpdateTimelineCommand
 *
 * @package Drupal\microblogging\Ajax
 */
class UpdateTimelineCommand implements CommandInterface {

  protected $status;

  protected $wrapper;

  protected $html;

  public function __construct($status, $wrapper, $html) {
    $this->status = $status;
    $this->wrapper = $wrapper;
    $this->html = $html;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    return [
      'command' => 'updateTimelineCommand',
      'status' => $this->status,
      'wrapper' => $this->wrapper,
      'html' => $this->html,
    ];
  }

}
