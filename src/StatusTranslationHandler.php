<?php

namespace Drupal\microblogging;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for status.
 */
class StatusTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
