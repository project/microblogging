<?php

namespace Drupal\microblogging;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Status entity.
 *
 * @see \Drupal\microblogging\Entity\Status.
 */
class StatusAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\microblogging\Entity\StatusInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished status entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published status entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit status entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete status entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add status entities');
  }


}
